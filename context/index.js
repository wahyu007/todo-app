import React, {createContext} from 'react';

const TaskContext = createContext({
  taskItems: [],
  setTaskItems: () => {},
});

export default TaskContext;