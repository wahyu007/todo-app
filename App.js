import React, {useState, useMemo} from 'react';
import TaskContext from './context';
import Task from './components/Task';

export default function App() {
  const [taskItems, setTaskItems] = useState([]);

  const value = useMemo(
    () => ({ taskItems, setTaskItems }),
    [taskItems]
  );

  return (
    <TaskContext.Provider value={value}>
      <Task />
    </TaskContext.Provider>
  );
}