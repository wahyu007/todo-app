import { nanoid } from 'nanoid';
import React, {useContext, useState} from 'react';
import { StyleSheet, Image, Text, View , KeyboardAvoidingView, TextInput, TouchableOpacity, ScrollView, Modal, Pressable, useColorScheme} from 'react-native';
import TaskContext from '../../context';
import TaskItem from './taskitem';
import SearchIcon from '../../assets/icons-search.svg';
import CloseIcon from '../../assets/close-icon.svg'


const Task = () => {
  const { taskItems, setTaskItems } = useContext(TaskContext);

  const [ searchResults, setSearchResults ] = useState([]);
  const [ searchVisible, setSearchVisible ] = useState(false);
  const [task, setTask] = useState({text: "", status: false, id: ""});
  const [modalVisible, setModalVisible] = useState(false);
  const [selectedTask, setSelectedTask] = useState({text: "", status: false, id: ""});
  const [inputText, setInputText] = useState('');
  const [inputIndex, setInputIndex] = useState('');
  const [searchText, setSearchText] = useState('');

  const handleAddTask = () => {
    setTaskItems([...taskItems, task]);
    setTask({text: ""});
    setTask({text: "", status: false, id: ""})
  }

  const completeTask = (index) => {
    let itemsCopy = [...taskItems];
    itemsCopy[index].status = !itemsCopy[index].status
    setTaskItems(itemsCopy);
  }

  const handleModal = (key, item) => {
    setInputIndex(key)
    setInputText(item.text)
    setSelectedTask(item)
    setModalVisible(true)
  }

  const deleteTask = (index) => {
    let itemsCopy = [...taskItems];
    itemsCopy.splice(index, 1);
    setTaskItems(itemsCopy);
  }

  const updateTask = () => {
    let itemsCopy = [...taskItems]
    itemsCopy[inputIndex].text = inputText
    console.log(itemsCopy[inputIndex].text);
    setTaskItems(itemsCopy);
    setModalVisible(!modalVisible);
  }

  const handleSearch = () => {
    let itemsCopy = [...taskItems];
    let results = itemsCopy.filter(
      el => el.text.includes(searchText)
    )
    setSearchResults(results);
    setSearchVisible(true);
    setSearchText('')
  }
  
  return (
    <View style={styles.container}>

      {/* modal */}

      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>Task Detail</Text>
            
            <TextInput 
              style={styles.input}
              value={inputText} 
              onChangeText={ (e) => setInputText(e)} 
              />

            <View style={styles.boxFlex}>
              <Pressable
                style={[styles.button, styles.buttonClose]}
                onPress={() => updateTask()}
              >
                <Text style={styles.textStyle}>Update</Text>
              </Pressable>
              <Pressable
                style={[styles.button, styles.buttonClose]}
                onPress={() => {
                  deleteTask(selectedTask.index2)
                  setModalVisible(!modalVisible)
                }}
              >
                <Text style={styles.textStyle}>Delete</Text>
              </Pressable>
              <Pressable
                style={[styles.button, styles.buttonClose]}
                onPress={() => setModalVisible(!modalVisible)}
              >
                <Text style={styles.textStyle}>Cancel</Text>
              </Pressable>
            </View>
          </View>
        </View>
      </Modal>

      {/* header or top */}

      <View style={styles.tasksWrapper}>
        <View style={styles.search}>
          <Text style={styles.sectionTitle}>Today's tasks</Text>
          <View style={styles.search}>
            <TextInput style={styles.inputHalf} 
              placeholder={'Search'} 
              value={searchText} 
              onChangeText={text => text != undefined && setSearchText(text)} />
            <TouchableOpacity onPress={() => handleSearch()}>
              <Image
                style={[styles.tinyLogo, searchVisible ? {display: 'none'} : {display: 'block'}]}
                source={SearchIcon}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => setSearchVisible(false)}>
              <Image
                style={[styles.tinyLogo, searchVisible ? {display: 'block'} : {display: 'none'}]}
                source={CloseIcon}
              />
            </TouchableOpacity>
          </View>
        </View>

        {/* items */}

        <ScrollView style={styles.scrollView}>
          <View style={styles.items}>
            {
              searchVisible ? 
              searchResults.map((item, index) => {
                return (
                  <TouchableOpacity key={item.id} onPress={() => handleModal(index, item)}>
                    <TaskItem item={item} completeTask={completeTask} index={index} />
                  </TouchableOpacity>
                )
              })
              :
              taskItems.map((item, index) => {
                return (
                  <TouchableOpacity key={item.id} onPress={() => handleModal(index, item)}>
                    <TaskItem item={item} completeTask={completeTask} index={index} />
                  </TouchableOpacity>
                )
              })
            }
          </View>
        </ScrollView>
      </View>

      {/* write a task */}

      <KeyboardAvoidingView
      // behavior={Platform.OS === "ios" ? "padding" : "height"}
      style={styles.writeTaskWrapper}>
        <TextInput style={styles.input} 
          placeholder={'Write a task'} 
          value={task.text} 
          onChangeText={text => text != undefined && setTask({text: text, status: false, id: 'task-'+nanoid()})} />
        <TouchableOpacity onPress={() => handleAddTask()}>
          <View style={styles.addWrapper}>
            <Text style={styles.addText}>+</Text>
          </View>
        </TouchableOpacity>
      </KeyboardAvoidingView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#E8EAED',
  },
  tasksWrapper : {
    paddingTop: 10,
    paddingHorizontal: 20,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: 'bold'
  },
  items: {
    marginTop: 30,
  },
  writeTaskWrapper: {
    position: 'absolute',
    bottom: 10,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  input: {
    paddingVertical: 15,
    paddingLeft: 20,
    backgroundColor: '#FFF',
    borderRadius: 60,
    borderColor: '#C0C0C0',
    borderWidth: 1,
    width: 300,
  },
  inputHalf: {
    paddingVertical: 15,
    paddingLeft: 20,
    backgroundColor: '#FFF',
    borderRadius: 60,
    borderColor: '#C0C0C0',
    borderWidth: 1,
    width: 150,
    height: 40,
  },
  addWrapper: {
    width: 60,
    height: 60,
    backgroundColor: '#FFF',
    borderRadius: 60,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 1,
  },
  coret: {
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid'
  },
  scrollView: {
    marginBottom: 110
  },
  addText: {
    fontSize: 20,
    fontWeight: 'bold'
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 15,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  button: {
    marginTop: 15,
    marginHorizontal: 5,
    width: 80,
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  },
  boxFlex: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  tinyLogo: {
    width: 35,
    height: 35,
    marginLeft: 5
  },
  search: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});

export default Task;