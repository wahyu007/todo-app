import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

const TaskItem = (props) => {
  return(
    <View style={styles.item}>
      <View style={styles.itemLeft}>
        <TouchableOpacity style={styles.square}></TouchableOpacity>
        <Text style={styles.itemText, props.item.status == 1 && styles.itemTextFinished }>{props.item.text}</Text>
      </View>
      <TouchableOpacity onPress={() => props.completeTask(props.index)}>
        <View style={[styles.circular, props.item.status == 1 && styles.circularSolid]}></View>
      </TouchableOpacity>
      
    </View>
  );
}

const styles = StyleSheet.create({
  item: {
    backgroundColor: '#FFF',
    padding: 15,
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 20,
  },
  itemLeft: {
    flexDirection: 'row',
    alignItems: 'center',
    flexWrap: 'wrap'
  },
  square: {
    width: 24,
    height: 24,
    backgroundColor: '#55BCF6',
    opacity: 0.4,
    borderRadius: 5,
    marginRight: 15,
  },
  itemText: {
    maxWidth: '80%',
  },
  itemTextFinished: {
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid'
  },
  circular: {
    width: 18,
    height: 18,
    borderColor: '#55BCF6',
    borderWidth: 2,
    borderRadius: 5,
  },
  circularSolid: {
    backgroundColor: '#55BCF6'
  }
});

export default TaskItem;